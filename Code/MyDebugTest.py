import pandas as pd
import numpy as np

df = pd.DataFrame(np.random.randint(0,
                                    100,
                                    size=(100,4)),
                  columns=list('ABCD'))
print(df)
#a program that replaces all values >50
# in A by 75, B by 80, C by 85 and D by 90

df.loc[df['A']>50, 'A']=75
df.loc[df['B']>50,'B']=80
df.loc[df['C']>50,'C']=85
df.loc[df['D']>50,'D']=90

print (df)