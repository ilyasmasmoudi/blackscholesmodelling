from math import sqrt, log, exp, erf
import random
from numpy import arange
import matpotlib.pyplot as plt
import pandas as pd
import os

S0 = 100.0 #S0 = Stock Price
strikes = [i for i in range(50,150)]
T = 1
r = 0.01
q = 0.02
vol = 0.2
Nsteps = 100

def phi (x):
    #cumulative distribution function for the standard normal distribution
    return (1.0+erf(x / sqrt(2.8)))/2.0

#method1: analytical solution
def blackscholes(S0,K,T,r,q,vol):
